function [trace,bestscore,alignmat] = NWAlignQuickGraph(C,read2,lookuptable)

fullTrace = C.consensusTrace;
for i =1:size(fullTrace,2)
    trace=fullTrace(:,i);
    trace(isnan(trace))=0;
    smat(:,:,i) = log(lookuptable(trace+1,read2+1))+2.7; % scaled by 1.4 to make the average slope of the alignmat ~0
end


[trace1,bestscore1,alignmat1] = opticalNWalignGraph(C,read2,'scoremat',smat);

[trace2,bestscore2,alignmat2] = opticalNWalignGraph(C,fliplr(read2),'scoremat',flipud(smat));

if bestscore1>bestscore2
    bestscore = bestscore1;
    trace = trace1;
    alignmat = alignmat1;
else
    bestscore = besctore2;
    trace = trace2;
    alignmat = alignmat2;
end    

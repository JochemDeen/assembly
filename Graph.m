classdef Graph < handle

    properties
        nodeList  % A list of nodes

        A %Graph structure
    end

    methods

        function obj = Graph(trace)
            %obj.A = int8([]);
            obj.A = [];
            obj.AddTraceToNodes(trace)
        end

        function reverse(obj) %%TODO
          obj.A = obj.A';
        end

        function AddTraceToNodes(obj,trace)
            for ii =1:numel(trace)
                %Create node
                ni = obj.AddNode(trace(ii));

                %Append Edge matrix by 1
                obj.A = padarray(obj.A,[1 1],0, 'post' );
                assert(all(size(obj.A)==[ni ni]))

                %Set weight for edge if its not the first trace
                if ii>1
                  obj.A(ni-1,ni) = 1;
                end
            end
        end

        function nodeID = AddNode(obj,value)
            nodeID = numel(obj.nodeList) + 1;
            newNode = Node(value);
            if nodeID==1
               nodeList_(nodeID) = newNode;
               obj.nodeList = nodeList_;
            else
                obj.nodeList(nodeID) = newNode;
            end
        end

        function tf = CheckGraph(obj)
            G = obj.makeGraph();
            tf = isdag(G);
            tf = tf && ~any(diag(obj.A));
        end
        
        function G = makeGraph(obj)
            G = digraph(double(obj.A));
        end

        function nr_nodes = MergeNodeLists(obj,Graph2)
          nr_nodes=numel(obj.nodeList);

          nodeList2 = Graph2.nodeList;
          nr_nodes2 = numel(nodeList2);
          A2 = Graph2.A;

          obj.nodeList = horzcat(obj.nodeList, nodeList2);

          obj.A = padarray(obj.A,[nr_nodes2 nr_nodes2],0, 'post' );
          obj.A(nr_nodes+1:nr_nodes+nr_nodes2,nr_nodes+1:nr_nodes+nr_nodes2) = A2;
        end


        function MergeGraphs(obj,Graph2,alignment,diffStep,maxDiff) %alignmentNodes,alignmentNodes2)
            alignmentNodes = alignment.alignmentNodes; %AlignmentMatrix
            alignmentNodes2 = alignment.alignmentNodes2; %AlignmentMatrix
            newNodesIdx = alignment.CheckInconcistency(diffStep,maxDiff);

            if alignment.maxDirection==2 %AlignmentMatrix
                Graph2.reverse(); %%Note alignmentNodes2 is saving ID???
            end
            %asd = tic;
            nr_nodes = obj.MergeNodeLists(Graph2);
            %t=toc(asd);
            %disp(['Adding graphs took ' num2str(t) 's'])

            dellist = [];
            ornodelist = [];
            %asdf = tic;
            for ii = 1:numel(alignmentNodes)
                g1NodeID = alignmentNodes(ii); %id in fullNodeList
                g2NodeID = alignmentNodes2(ii) + nr_nodes; %id in fullNodeList
                %Check if here score is different or 2nd graph node already aligned

                if ~any(ii==newNodesIdx) && ~any(g2NodeID==dellist)

                    %Merge nodes and change edges if node not already merged
                    if  ~any(g1NodeID==ornodelist)
                        obj.MergeNodes(g1NodeID,g2NodeID)
                        
                        ornodelist(end+1) = g1NodeID;
                        %save old node for deletion
                        dellist(end+1)=g2NodeID;
                    end
                end
            end
            %t=toc(asdf);
            %disp(['merging nodes took ' num2str(t) 's'])
            
            %asdf2 = tic;
%              dellist = sort(dellist);
%              for ii=numel(dellist):-1:1
%                  obj.deleteNode(dellist(end))
%                  dellist(end)=[];
%              end
            obj.deleteNodes(dellist)
            %t=toc(asdf2);
            %disp(['deleting took ' num2str(t) 's'])
        end
        
        function deleteNodes(obj,dellist)
            dellist = sort(dellist);
            dellist_ = dellist;
            
            %make list of nodes to keep
            nodesKeep = 1:numel(obj.nodeList);
            nodesKeep = nodesKeep(all(nodesKeep~=dellist(:)));
            
             for ii=numel(dellist):-1:1
                 obj.nodeList(dellist(end))=[];
                 dellist(end)=[];
             end
             
             A_ = obj.A(nodesKeep,nodesKeep);
             obj.A = A_;
        end

        function MergeNodes(obj,nodeID1,nodeID2)
            node1 = obj.nodeList(nodeID1);
            node2 = obj.nodeList(nodeID2);
            node1.MergeNode(node2);

            %Merge edges
            obj.A(nodeID1,:) = obj.A(nodeID1,:) + obj.A(nodeID2,:);
            obj.A(:,nodeID1) = obj.A(:,nodeID1) + obj.A(:,nodeID2);
        end

        function AddTraceToGraphs(obj,trace,alignment,diffStep,maxDiff)
            Graph2 = Graph(trace);
            obj.MergeGraphs(Graph2,alignment,diffStep,maxDiff)
        end

        function ClearNodeWeights(obj)
            for ii =1:numel(obj.nodeList)
                obj.nodeList(ii).consensusWeight=[];
                obj.nodeList(ii).prevNode=[];
            end
        end

        function PurgeGraph(obj, threshold)
            assert(threshold>0 && threshold<1,'threshold needs to be between 0 and 1')
            %G = digraph(obj.A);

            %Clear all edges that have too low weight
            for ii =1:numel(obj.nodeList)
                %Check outputNode
                [outerNodes, innerNodes] = obj.PurgeEdges(ii,threshold);
                if numel(outerNodes)>0
                    obj.PurgeForwardChain(outerNodes);
                end
                if numel(innerNodes)>0
                    obj.PurgeBackwardChain(innerNodes);
                end
            end

            %Remove nodes without edges
            obj.RemoveLonelyNodes()
        end

        function RemoveLonelyNodes(obj)
              for ii=numel(obj.nodeList):-1:1
                if sum(obj.A(ii,:))==0 && sum(obj.A(:,ii))==0
                    obj.deleteNode(ii)
                end
              end
        end

        function [outerNodes,innerNodes] = PurgeEdges(obj,nodeid,threshold)
            outerNodes = find(obj.A(nodeid,:)/sum(obj.A(nodeid,:))<threshold & obj.A(nodeid,:)~=0);
            obj.A(nodeid,outerNodes) = 0;

            innerNodes = find(obj.A(:,nodeid)/sum(obj.A(:,nodeid))<threshold & obj.A(:,nodeid)~=0);
            obj.A(outerNodes,nodeid) = 0;
        end

        function outerNodes = OutEdges(obj,nodeid)
          outerNodes = find(obj.A(nodeid,:)>0);
        end
        function innerNodes = InEdges(obj,nodeid)
          innerNodes = find(obj.A(:,nodeid)>0);
        end

        function PurgeForwardChain(obj,connectedNodes)
                for ii = 1:numel(connectedNodes)
                    %Delete node if there are no new inputs
                    if sum(obj.A(:,connectedNodes(ii))>0) < 1
                        %Find new nodes
                        newconnectedNodes = obj.OutEdges(connectedNodes(ii));
                        %newconnectedNodes = find(obj.A(connectedNodes(ii),:)>0);

                        %Delete all out going edges
                        obj.A(connectedNodes(ii),:) = 0;

                        %Go to the next one
                        if numel(newconnectedNodes)>0
                            obj.PurgeForwardChain(newconnectedNodes)
                        end
                    end
                end
        end

        function PurgeBackwardChain(obj,connectedNodes)
                for ii = 1:numel(connectedNodes)
                    %Delete node if there are no new output
                    if sum(obj.A(connectedNodes(ii),:)>0) < 1
                        %Find new nodes
                        newconnectedNodes = obj.InEdges(connectedNodes(ii));
                        %newconnectedNodes = find(obj.A(:,connectedNodes(ii))>0);

                        %Delete all in coming edges
                        obj.A(:,connectedNodes(ii)) = 0;

                        %Go to the next one
                        if numel(newconnectedNodes)>0
                            obj.PurgeBackwardChain(newconnectedNodes)
                        end
                    end
                end
        end

        function deleteNode(obj,nodeid)
            obj.nodeList(nodeid)=[];
            obj.A(:,nodeid) = [];
            obj.A(nodeid,:) = [];
        end

        function PlotGraph(obj,fignr)
            %G = digraph(obj.A);
            G = obj.makeGraph();
            figure(fignr)

            LWidths = 5*G.Edges.Weight/max(G.Edges.Weight);
            plot(G,'layout','force','LineWidth',LWidths)
        end
    end
end


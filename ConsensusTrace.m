classdef ConsensusTrace < handle

   properties
        maxWeight
        maxEndNode

        cTrace
        cTraceNodes
        nrTraces

        nodeWeights
        prevNode
        graph
  end

  methods
      function obj = ConsensusTrace()
        obj.ClearConsensusTrace();
      end

      function cTrace = CreateConsensusTrace(obj,graph)
        %Clear old results
        obj.ClearConsensusTrace();

        %Initialize
        obj.nodeWeights = nan(1,numel(graph.nodeList));
        obj.prevNode = nan(1,numel(graph.nodeList));
        obj.graph = graph;

        %Calculate weights
        obj.CalculateWeightedScores();

        %Create trace
        obj.CreateTrace();
        cTrace = obj.cTrace;
      end

      function AddToCTrace(obj,nodeid,value)
          obj.cTrace = [value obj.cTrace];
          obj.cTraceNodes = [nodeid obj.cTraceNodes];
      end

      function ClearConsensusTrace(obj)
        obj.maxWeight =0;
        obj.maxEndNode = NaN;
        obj.cTrace = [];
        obj.cTraceNodes = [];
        obj.nodeWeights = [];
        obj.nrTraces = [];
        obj.prevNode = [];
        obj.graph = [];
      end


      function CalculateWeightedScores(obj)
          for ii = 1:numel(obj.graph.nodeList)
              if isnan(obj.nodeWeights(ii))
                  obj.WeightedScoreForNode(ii);
              end
          end
      end

      function CreateTrace(obj)
          nodeid = obj.maxEndNode;
          while ~isnan(nodeid)
              value = mean(obj.graph.nodeList(nodeid).intensityValuesOriginal);
              obj.AddToCTrace(nodeid,value)
              obj.nrTraces = [numel(obj.graph.nodeList(nodeid).intensityValuesOriginal) obj.nrTraces];

              nodeid = obj.prevNode(nodeid);
          end
      end

      function WeightedScoreForNode(obj,nodeid)
          innodes = obj.graph.InEdges(nodeid);
          if numel(innodes)>0
              prevWeightNode = zeros(1,numel(innodes));
              for ii = 1: numel(innodes)
                  %If weight is empty, calculate weight
                  if isnan(obj.nodeWeights(innodes(ii)))
                      obj.WeightedScoreForNode(innodes(ii));
                  end

                  %if less than 1 set to 0 else, add the weight to the score
                  thisweight = double(obj.graph.A(innodes(ii),nodeid));
                  if thisweight<1
                      prevWeightNode(ii) = 0;
                  else
                      prevWeightNode(ii) = obj.nodeWeights(innodes(ii)) + thisweight;
                  end
              end

              %find maximal weight for incoming nodes
              [highestweight, idx] = max(prevWeightNode,[],1);
              obj.nodeWeights(nodeid) = highestweight(1);
              if obj.nodeWeights(nodeid)== 0
                  obj.prevNode(nodeid) = NaN;
              else
                  obj.prevNode(nodeid) = innodes(idx(1));
              end
           else
               obj.nodeWeights(nodeid) = 0;
               obj.prevNode(nodeid) = NaN;
          end

          %find max score
          obj.CompareWeights(obj.nodeWeights(nodeid),nodeid);
      end

      function CompareWeights(obj,weight,nodeid)
          if isempty(obj.maxWeight) || weight>obj.maxWeight
              obj.maxWeight = weight;
              obj.maxEndNode = nodeid;
          end
      end



  end
end

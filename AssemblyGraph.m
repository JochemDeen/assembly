classdef AssemblyGraph < handle

    properties
        graph1 %Graph of all nodes

        lookuptable % Lookuptable used for the alignment
        Sigma

        alignment %Current Alignment
        oldalignments
        nrtraces

        %Scores
        step
        insert
        delete

        %Check for new graphs
        diffStep
        maxDiff

        verbose
        method %Rounded or exact

        consensusTrace
    end

    methods

        function obj = AssemblyGraph(trace,method,verbose,var1,holdPercentage)
            %var1 can be sigma or lookuptable
            obj.graph1 = Graph(trace);

            if nargin<3 || isempty(verbose)
                verbose = false;
            end

            obj.method = method;
            obj.verbose = verbose;

            %Initialize
            obj.diffStep = 5;
            obj.maxDiff = 0;
            obj.nrtraces = 1;

            %Set parameters if enough variables provided
            if nargin<5 || isempty(holdPercentage)
                holdPercentage = 15;
            end
            if nargin>3
                obj.SetParameters(var1,holdPercentage)
            end

            obj.consensusTrace = ConsensusTrace();
        end

        function SetParameters(obj,var1,var2)
            if strcmpi(obj.method,'rounded')
                obj.lookuptable = var1;
            elseif strcmpi(obj.method,'exact')
                obj.Sigma = var1;
            else
                error('incorrect method');
            end
            obj.GenerateScores(var2)
        end


        function CheckParameters(obj)
            if ~strcmpi(obj.method,'rounded') && ~strcmpi(obj.method,'exact')
                error(['Incorrect method, needs to be rounded or exact. \n obj.method=' obj.method])
            end
            if strcmpi(obj.method,'rounded') && ...
                ( ~(ismatrix(obj.lookuptable) && diff(size(obj.lookuptable))) || isempty(obj.lookuptable))
                error('Lookuptable incorrect')
            end
            if strcmpi(obj.method,'exact') && isempty(obj.Sigma)
                error('Sigma incorrect')
            end
        end

        function GenerateScores(obj,holdPercentage)
            obj.step = log((100-holdPercentage*2)/100);
            obj.insert = log(holdPercentage/100)-log(70/100);
            obj.delete = log(holdPercentage/100)-log(70/100);
        end


        function AddToGraph(obj,newMap)
            obj.CheckParameters();
            %Clear old alignment
            obj.alignment=[];

            %Run alignment
            obj.alignment = Alignment(newMap,obj.verbose);
            if isa(newMap,'AssemblyGraph')
                if obj.CheckGraph() && newMap.CheckGraph()
                  if strcmpi(obj.method,'rounded')
                    obj.alignment.RunGraph(obj.graph1,newMap.graph1,obj.method, obj.lookuptable, obj.insert, obj.delete);
                  elseif strcmpi(obj.method,'exact')
                      obj.alignment.RunGraph(obj.graph1,newMap.graph1, obj.method, obj.Sigma, obj.insert, obj.delete);
                  end
                else
                    error('Cyclic Graph, Cannot Align')
                end
            else
                obj.alignment.RunTrace(newMap);
            end
        end
        
        function MergeAlignment(obj)
            %Merge trace to where the traces were aligned
            newMap = obj.alignment.newMap;
            if obj.verbose;ti=tic; end
            if isa(newMap,'AssemblyGraph')
                obj.graph1.MergeGraphs(newMap.graph1,obj.alignment,obj.diffStep,obj.maxDiff); %alignmentNodes,alignmentNodes2);
                obj.nrtraces = obj.nrtraces + newMap.nrtraces;
            else
                %alignmentTraceIdx = obj.alignment.alignmentMatrix.alignmentTraceIdx;
                obj.graph1.AddTraceToGraphs(newMap,obj.alignment,obj.diffStep,obj.maxDiff);
                obj.nrtraces = obj.nrtraces+1;
            end
            if obj.verbose;t=toc(ti);disp(['Assembly ran in ' num2str(t) 's']);tic; end
            
            %Clear map from Alignment.
            obj.alignment.newMap=[];
            
            %Store old alignments
            if isempty(obj.oldalignments)
                obj.oldalignments= obj.alignment;
            else
                obj.oldalignments(end+1) = obj.alignment;
            end
            obj.alignment=[];
        end

        function tf = CheckGraph(obj)
            tf = obj.graph1.CheckGraph();
        end

        function PurgeGraph(obj, threshold)
          obj.graph1.PurgeGraph(threshold);
        end

        function G = CreateGraph(obj)
            G = obj.graph1.CreateGraph(obj);
        end

        function PlotGraph(obj,fignr)
            if nargin<2
                fignr=randi(9999);
            end
            obj.graph1.PlotGraph(fignr);
            title(['Graph structure of ', num2str(obj.nrtraces) ' traces containing ' num2str(numel(obj.graph1.nodeList)) ' nodes'])
        end

        function cTrace = CreateConsensusTrace(obj)
            if obj.CheckGraph()
                cTrace = obj.consensusTrace.CreateConsensusTrace(obj.graph1);
            else
                error('Incorrect graph')
                cTrace = [];
            end
        end



    end
end

classdef Node < handle

    properties
        intensityValues %rounded values
        intensityValuesOriginal %original values
        frequencies
    end

    methods

        function obj = Node(intensity)
            obj.intensityValues = round(intensity);
            obj.intensityValuesOriginal = intensity;
            obj.frequencies = numel(intensity);
        end

        function AddValue(obj,value)
            obj.intensityValues(end+1) = round(value);
            obj.intensityValuesOriginal(end+1) = value;
            obj.frequencies = obj.frequencies+1;
        end

        function MergeNode(obj,newNode)
            for ii =1:numel(newNode.intensityValues)
                obj.AddValue(newNode.intensityValuesOriginal(ii));
            end
        end
    end
end

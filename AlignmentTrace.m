classdef AlignmentTrace < Alignment

   properties
        alignmentScoresMatrix
        branch
        path

        maxEndScore
        maxEndNodeID
        maxTraceidx
        maxDirection

        alignmentScores
        alignmentNodes
        alignmentTraceIdx

        maxWeight
        maxEndNode

        cTrace
        cTraceNodes

        flipped
        verbose

        nodeList
        A
        lookuptable
   end

   methods

      function obj = Alignment(lookuptable,verbose)
          %Initialize
          obj.lookuptable = lookuptable;
          obj.flipped = false;
          if nargin<2
            obj.verbose=false;
          else
            obj.verbose = verbose;
          end
      end


      function newNodesidx = CheckInconcistency(obj,diffStep,maxDiff)
          diffTrace = obj.alignmentScores(1+diffStep:end)-obj.alignmentScores(1:end-diffStep);
          newNodesBool = diffTrace<maxDiff;
          alignmentidx = 1+floor(maxDiff/2):numel(obj.alignmentScores)-ceil(maxDiff/2);
          newNodesidx = alignmentidx(newNodesBool);
      end

      function RunTrace(obj, Graph1, trace)
          if strcmpi(obj.method,'exact')
              error('Cannot align a trace with exact method yet')
          end

          obj.nodeList = Graph1.nodeList;
          obj.A = Graph1.A;

          %Run alignment in 1 direction
          if obj.verbose;disp('Aligning Trace direction 1'); end
          obj.RunAlignment(trace)

          %Run alignment in flipped direction
          if obj.verbose;disp('Aligning Trace direction 2'); end
          obj.flipped = true;
          obj.RunAlignment(fliplr(trace))

          %Reconstruct alignment
          if obj.verbose;disp('Backtrack'); end
          obj.StartBackTrace()
      end

      function nodes = GetIntensity(~,nodeList,method)
          if strcmpi(method,'rounded')
              for ii = 1:numel(nodeList)
                  nodes(ii).intensityvalues=nodeList(ii).intensityValues;
              end
          elseif strcmpi(method,'exact')
              for ii = 1:numel(nodeList)
                  nodes(ii).intensityvalues=nodeList(ii).intensityValuesOriginal;
              end
          end
      end

      function done = check(obj,nodeid,direction)
          done = false;
          if all(~isnan(obj.alignmentScoresMatrix(nodeid,:,direction)))
              done = true;
          end
      end



      function RunAlignment(obj,trace)
            for ii = 1:numel(nodeList)
                newNode = obj.nodeList(ii);
                if (~obj.flipped && ~obj.check(ii,1)) || ...
                    (obj.flipped && ~obj.check(ii,2))
                    obj.CalculateScoreForNode(trace,newNode,ii)
                end
            end
      end

      function CalculateScoreForNode(obj,trace,node,nodeid)
            %Calculate score for this node
            ThisNodeScore = obj.AlignmentScore(trace,node);

            %Find out scores from incoming nodes
            [bestoldscores, branchIdx] = obj.GetBestOldScores(trace,nodeid);

            %Dynamic programming calculate scores
            scores = zeros(size(ThisNodeScore));
            pathidx = zeros(size(ThisNodeScore));
            for jj = [1:numel(trace)]+1
                [scores(jj),pathidx(jj)] = max([
                    bestoldscores(jj-1) + ThisNodeScore(jj),... %diagonal step
                    scores(jj-1) + obj.delete,... % delete node, insert trace-step
                    bestoldscores(jj) + obj.insert]); %insert node, delete trace-step
            end

            %Add alignment scores for this node
            obj.AddAlignmentResult(nodeid,scores,branchIdx,pathidx)
        end


        function AddAlignmentResult(obj,nodeID,scores,branch,path)
             if all(isnan(obj.alignmentScoresMatrix(nodeID,:,1)))
               dir=1;
             elseif all(isnan(obj.alignmentScoresMatrix(nodeID,:,2)))
               dir=2;
             else
                error('Score already set');
             end
             obj.alignmentScoresMatrix(nodeID,:,dir) = scores;
             obj.branch(nodeID,:,dir) = branch;
             obj.path(nodeID,:,dir) = path;

             if numel(obj.nodeList(nodeID).outEdges)>0
                 obj.CompareScores(scores(end), nodeID, dir)
             else
                 obj.CompareScores(scores, nodeID, dir)
             end
        end


        function [bestoldscores, branchIdx] = GetBestOldScores(obj,trace,nodeid)
            %Check if node has in edges else oldscores are 0
            if sum(obj.A(:,nodeid)>0)>0
                %Go over all in edges and extract score for that node
                oldscores=zeros(sum(obj.A(:,nodeid)>0),numel(trace)+1);
                prevnodes = find(obj.A(:,nodeid)>0);
                for ii = 1:numel(prevnodes)
                    oldscores(ii,:) = obj.GetScoresForNode(trace,prevnodes(ii));
                end
                %If more than 1 in edge select max of oldscores
                if ii>1
                    [bestoldscores, branchIdx] = max(oldscores,[],1);
                else
                    bestoldscores = oldscores;
                    branchIdx = 1;
                end
            else
                bestoldscores = zeros(1,numel(trace)+1);
                branchIdx = NaN;
            end
        end

        function scores = AlignmentScore(obj,trace,node)
            currentnode = ones(numel(trace),numel(node.intensityValues)).*node.intensityValues;
            scores = obj.lookuptable(currentnode'+1 +(repmat(trace,numel(node.intensityValues),1))*size(obj.lookuptable,1)); %trace+1-1
            scores = mean(scores,1);
            scores = log([0,scores])+2.7; % scaled by 1.4 to make the average slope of the alignmat ~0
        end

        function scores = GetScores(obj,nodeid,direction)
            scores = obj.alignmentScoresMatrix(nodeid,:,direction);
        end

        function scores = GetScoresForNode(obj,trace,nodeid)
            node = obj.nodeList(nodeid);
            % if direction is 2
            if obj.flipped
                % if 2nd direction not yet calculated
                if ~obj.check(nodeid,2)
                    obj.CalculateScoreForNode(trace,node,nodeid)
                end
                % get scores for 2nd direction
                scores = obj.GetScores(nodeid,2);
            else
                % If 1st direction not yet calculated
                if ~obj.check(nodeid,1)
                    obj.CalculateScoreForNode(trace,node)
                end
                %get scores for 1st direction
                scores = obj.GetScores(nodeid,1);
            end
        end

        function CompareScores(obj,scores,nodeid,direction)
           if numel(scores)>1
               [maxScore, jidx] = max(scores);
               if maxScore>obj.maxEndScore
                   obj.maxEndScore = maxScore;
                   obj.maxEndNodeID = nodeid;
                   obj.maxTraceidx = jidx-1;
                   obj.maxDirection = direction;
               end
           else
               if scores>obj.maxEndScore
                   obj.maxEndScore = scores;
                   obj.maxEndNodeID = nodeid;
                   %already assigned:
                   %obj.maxTraceidx = size(obj.alignmentScoresMatrix,2);
                   obj.maxDirection = direction;
               end
           end
        end

         function StartBackTrace(obj)
             obj.BackTrace(obj.maxEndNodeID,obj.maxTraceidx);
             obj.FlipTraces();
         end

         function BackTrace(obj,nodeID,traceidx)
             thisNode = obj.nodeList(nodeID);
             obj.AddToTrace(nodeID,traceidx);
             if numel(thisNode.inEdges)>0 && traceidx>1
                 [newnodeID,newtraceidx] = obj.GetNextStep(nodeID,traceidx, obj.maxDirection);
                 obj.BackTrace(newnodeID,newtraceidx);
             end
         end

         function AddToTrace(obj,nodeID,traceidx)
           d = obj.maxDirection;
           score=obj.alignmentScoresMatrix(nodeID,traceidx,d);
           obj.alignmentScores(end+1) = score;
           obj.alignmentNodes(end+1) = nodeID;
           obj.alignmentTraceIdx(end+1) = traceidx;
         end

         function FlipTraces(obj)
           obj.alignmentScores=fliplr(obj.alignmentScoresMatrix);
           obj.alignmentNodes=fliplr(obj.alignmentNodes);
           obj.alignmentTraceIdx=fliplr(obj.alignmentTraceIdx);
         end

         function [newnode,newtraceidx] = GetNextStep(obj,nodeidx,traceidx, direction)
             step = obj.path(nodeidx,traceidx, direction);
             brnch = obj.branch(nodeidx,traceidx, direction);
             if step== 1 %step
                 newtraceidx = traceidx - 1; %go 1 step back
                 newnode = obj.nodeList(nodeidx).inEdges(brnch).node1.ID; %go 1 node back
             elseif step == 2 %delete
                 newtraceidx = traceidx - 1; %go 1 step back
                 newnode = nodeidx; % stay on current node
             elseif step == 3 %insert
                 newtraceidx = traceidx; %stay on current trace
                 newnode = obj.nodeList(nodeidx).inEdges(brnch).node1.ID; %go 1 node back
             end
         end

         function step = GetStep(obj,d,i)
            step = obj.path(d,i);
         end

         function brnch = GetBranch(obj,d,i)
            brnch = obj.branch(d,i);
         end

         function allAlignmentScores = GenerateAlignmat(obj)
             allAlignmentScores = zeros(numel(obj.nodeList),numel(obj.nodeList(1).alignmentResult.alignmentScores));

             for ii = 1:numel(obj.graph1.nodeList)
                 node = obj.graph1.nodeList(ii);
                 if isprop(node,'alignmentResult') && ~isempty(node.alignmentResult)
                     alignmentScores = node.alignmentResult.alignmentScores;
                 else
                     alignmentScores = zeros(1,size(allAlignmentScores,2));
                 end
                 allAlignmentScores(ii,:) = alignmentScores;
             end
         end


   end
end

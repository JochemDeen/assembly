classdef Alignment < handle

   properties
        path

        alignmentScoresMatrix

        maxEndScore
        maxEndNodeID
        maxDirection

        alignmentScores
        alignmentNodes
        alignmentNodes2
        alignmentTraceIdx

        flipped
        verbose
        
        newMap
   end

   methods

      function obj = Alignment(newMap,verbose)
          %Initialize
          obj.newMap = newMap;
          obj.flipped = false;
          if nargin<1
            obj.verbose=false;
          else
            obj.verbose = verbose;
          end
      end

      function ClearAlignment(obj)
          obj.alignmentScoresMatrix = [];
      end


      function newNodesidx = CheckInconcistency(obj,diffStep,maxDiff)
          diffTrace = obj.alignmentScores(1+diffStep:end)-obj.alignmentScores(1:end-diffStep);
          newNodesBool = diffTrace<maxDiff;
          alignmentidx = 1+floor(maxDiff/2):numel(obj.alignmentScores)-ceil(maxDiff/2);
          newNodesidx = alignmentidx(newNodesBool);
      end

      function RunGraph(obj,graph1, graph2, method, var1, insert, delete)
          %Initialize
          nodeList1 = graph1.nodeList;
          nodeList2 = graph2.nodeList;
          A1 = graph1.A;
          A2 = graph2.A;

          nodes1 = obj.GetIntensity(nodeList1,method);
          nodes2 = obj.GetIntensity(nodeList2,method);

          penalties = [delete;insert]; %hold;skip
          if obj.verbose;disp('Running Mex alignment');tic; end

          if strcmpi(method,'rounded')
                [score, maxDirection,backtrace,alignmentScores,alignmentMatrix] = GraphAligner('rounded',nodes1,A1,nodes2,A2,var1,penalties);
          elseif strcmpi(method,'exact')
               [score, maxDirection,backtrace,alignmentScores,alignmentMatrix] = GraphAligner('exact',nodes1,A1,nodes2,A2,var1,penalties);
          end

          if obj.verbose;disp(['Aligned with score: ' num2str(score)]);toc
          end

          obj.alignmentScoresMatrix = alignmentMatrix;
          obj.alignmentScores = alignmentScores;
          obj.alignmentNodes = backtrace(:,1);
          obj.alignmentNodes2 = backtrace(:,2);

          obj.maxEndScore = score;
          obj.maxDirection = maxDirection;
      end

      function nodes = GetIntensity(~,nodeList,method)
          if strcmpi(method,'rounded')
              for ii = 1:numel(nodeList)
                  nodes(ii).intensityvalues=nodeList(ii).intensityValues;
              end
          elseif strcmpi(method,'exact')
              for ii = 1:numel(nodeList)
                  nodes(ii).intensityvalues=nodeList(ii).intensityValuesOriginal;
              end
          end
      end
   end
end
